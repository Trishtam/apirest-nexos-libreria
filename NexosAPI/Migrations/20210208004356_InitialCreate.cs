﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NexosAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Autores",
                columns: table => new
                {
                    IdAutor = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreAutor = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    FechaNacimiento = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    CiudadAutor = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    CorreoAutor = table.Column<string>(type: "nvarchar (100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Autores", x => x.IdAutor);
                });

            migrationBuilder.CreateTable(
                name: "Editorial",
                columns: table => new
                {
                    IdEditorial = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreEditorial = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    DireccionEditorial = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    TelefonoEditorial = table.Column<string>(type: "nvarchar (20)", nullable: true),
                    CorreoEditorial = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    MaxLibRegistrado = table.Column<int>(type: "int ", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Editorial", x => x.IdEditorial);
                });

            migrationBuilder.CreateTable(
                name: "Libro",
                columns: table => new
                {
                    LibroId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titulo = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    Año = table.Column<int>(type: "int", nullable: false),
                    Genero = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    NumPaginas = table.Column<int>(type: "int", nullable: false),
                    Editorial = table.Column<string>(type: "nvarchar (100)", nullable: true),
                    Autor = table.Column<string>(type: "nvarchar (100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Libro", x => x.LibroId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Autores");

            migrationBuilder.DropTable(
                name: "Editorial");

            migrationBuilder.DropTable(
                name: "Libro");
        }
    }
}
