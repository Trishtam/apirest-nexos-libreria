﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NexosAPI.Models;

namespace NexosAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EditorialController : ControllerBase
    {
        private readonly DataContext _context;

        public EditorialController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Editorial
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Editoriales>>> GetEditorial()
        {
            return await _context.Editorial.ToListAsync();
        }

        // GET: api/Editorial/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Editoriales>> GetEditoriales(int id)
        {
            var editoriales = await _context.Editorial.FindAsync(id);

            if (editoriales == null)
            {
                return NotFound();
            }

            return editoriales;
        }

        // PUT: api/Editorial/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEditoriales(int id, Editoriales editoriales)
        {
            if (id != editoriales.IdEditorial)
            {
                return BadRequest();
            }

            _context.Entry(editoriales).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EditorialesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return NoContent();
            return new JsonResult("Editorial Actualizada");
        }

        // POST: api/Editorial
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Editoriales>> PostEditoriales(Editoriales editoriales)
        {
            if (editoriales.MaxLibRegistrado== 0 )
            {
                editoriales.MaxLibRegistrado = -1;
            }
            
            _context.Editorial.Add(editoriales);
            await _context.SaveChangesAsync();
            return new JsonResult("Editorial Ingresada");
        }

        // DELETE: api/Editorial/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEditoriales(int id)
        {
            var editoriales = await _context.Editorial.FindAsync(id);
            if (editoriales == null)
            {
                return NotFound();
            }

            _context.Editorial.Remove(editoriales);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool EditorialesExists(int id)
        {
            return _context.Editorial.Any(e => e.IdEditorial == id);
        }
    }
}
