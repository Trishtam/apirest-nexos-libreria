﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NexosAPI.Models
{
    public class Editoriales
    {
        [Key]
        public int IdEditorial { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string NombreEditorial { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string DireccionEditorial { get; set; }

        [Column(TypeName = "nvarchar (20)")]
        public string TelefonoEditorial { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string CorreoEditorial { get; set; }

        [Column(TypeName = "int ")]
        public int MaxLibRegistrado { get; set; }
        
    }
}
