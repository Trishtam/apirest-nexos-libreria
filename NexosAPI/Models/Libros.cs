﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NexosAPI.Models
{
    public class Libros
    {
        [Key]
        public int LibroId { get; set; }
        [Column(TypeName = "nvarchar (100)")]
        public string  Titulo { get; set; }

        [Column(TypeName = "int")]
        public int Año { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string Genero { get; set; }

        [Column(TypeName = "int")]
        public int NumPaginas { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string Editorial { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string Autor { get; set; }

    }
}
