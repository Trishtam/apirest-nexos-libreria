﻿using Microsoft.EntityFrameworkCore;

namespace NexosAPI.Models
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }

        public DbSet<Autores> Autores { get; set; }
        public DbSet<Editoriales> Editorial { get; set; }
        public DbSet<Libros> Libro { get; set; }

    }
}
