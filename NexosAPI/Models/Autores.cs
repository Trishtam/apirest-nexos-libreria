﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NexosAPI.Models
{
    public class Autores
    {
        [Key]
        public int IdAutor { get; set; }
        
        [Column(TypeName = "nvarchar (100)")]
        public string NombreAutor { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string FechaNacimiento { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string CiudadAutor { get; set; }

        [Column(TypeName = "nvarchar (100)")]
        public string CorreoAutor { get; set; }



    }
}
