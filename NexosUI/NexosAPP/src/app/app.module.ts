import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LibrosComponent } from './libros/libros.component';
import { AutoresComponent } from './autores/autores.component';
import { EditorialesComponent } from './editoriales/editoriales.component';
import { ShowAutoresComponent } from './autores/show-autores/show-autores.component';
import { AddEditAutoresComponent } from './autores/add-edit-autores/add-edit-autores.component';
import { ShowEditorialesComponent } from './editoriales/show-editoriales/show-editoriales.component';
import { AddEditEditorialesComponent } from './editoriales/add-edit-editoriales/add-edit-editoriales.component';
import { ShowLibrosComponent } from './libros/show-libros/show-libros.component';
import { AddEditLibrosComponent } from './libros/add-edit-libros/add-edit-libros.component';
import { SharedService } from './shared.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LibrosComponent,
    AutoresComponent,
    EditorialesComponent,    
    ShowAutoresComponent,    
    AddEditAutoresComponent,
    ShowEditorialesComponent,
    AddEditEditorialesComponent,
    ShowLibrosComponent,
    AddEditLibrosComponent
  ],
  imports: [
    BrowserModule,    
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
