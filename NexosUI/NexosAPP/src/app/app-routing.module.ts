import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LibrosComponent } from "./libros/libros.component";
import { AutoresComponent } from "./autores/autores.component";
import { EditorialesComponent } from "./editoriales/editoriales.component";

const routes: Routes =[
{path:'Libro',component:LibrosComponent},
{path:'Editorial',component:AutoresComponent},
{path:'Autor',component:EditorialesComponent}
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]

})
export class AppRoutingModule{}