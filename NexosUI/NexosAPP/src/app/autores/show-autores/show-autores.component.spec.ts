import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAutoresComponent } from './show-autores.component';

describe('ShowAutoresComponent', () => {
  let component: ShowAutoresComponent;
  let fixture: ComponentFixture<ShowAutoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowAutoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAutoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
