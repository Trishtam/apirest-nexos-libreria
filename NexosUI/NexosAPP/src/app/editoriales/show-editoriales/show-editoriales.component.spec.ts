import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEditorialesComponent } from './show-editoriales.component';

describe('ShowEditorialesComponent', () => {
  let component: ShowEditorialesComponent;
  let fixture: ComponentFixture<ShowEditorialesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEditorialesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEditorialesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
