import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LibroComponent } from './libro/libro.component';
import { ShowLibroComponent } from './libro/show-libro/show-libro.component';
import { AddEditLibroComponent } from './libro/add-edit-libro/add-edit-libro.component';
import { AutorComponent } from './autor/autor.component';
import { ShowAutorComponent } from './autor/show-autor/show-autor.component';
import { AddEditAutorComponent } from './autor/add-edit-autor/add-edit-autor.component';
import { EditorialComponent } from './editorial/editorial.component';
import { ShowEditorialComponent } from './editorial/show-editorial/show-editorial.component';
import { AddEditEditorialComponent } from './editorial/add-edit-editorial/add-edit-editorial.component';

import { SharedService } from "./shared.service";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    LibroComponent,
    ShowLibroComponent,
    AddEditLibroComponent,
    AutorComponent,
    ShowAutorComponent,
    AddEditAutorComponent,
    EditorialComponent,
    ShowEditorialComponent,
    AddEditEditorialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
