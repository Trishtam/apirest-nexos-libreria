import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl='https://localhost:44365/api';

  constructor(private http:HttpClient) { }

  /*crud Libros*/
  getLibList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Libro')
  }

  addLib(val:any){
    return this.http.post(this.APIUrl+'/Libro',val)
  }

  updtLib(val:any){
    return this.http.put(this.APIUrl+'/Libro',val)
  }

  deletLib(val:any){
    return this.http.delete(this.APIUrl+'/Libro/'+val)
  }

/*crud Editorial*/

  getEditList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Editorial')
  }

  addEdit(val:any){
    return this.http.post(this.APIUrl+'/Editorial',val)
  }

  updtEdit(val:any){
    return this.http.put(this.APIUrl+'/Editorial',val)
  }

  deletEdit(val:any){
    return this.http.delete(this.APIUrl+'/Editorial/'+val)
  }

  getAutList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/Autor')
  }

  /*crud autor*/

  addAut(val:any){
    return this.http.post(this.APIUrl+'/Autor',val)
  }

  updtAut(val:any){
    return this.http.put(this.APIUrl+'/Autor',val)
  }

  deletAut(val:any){
    return this.http.delete(this.APIUrl+'/Autor/'+val)
  }

  getAllLibroNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/Libro/GetLibro')
  }

}
