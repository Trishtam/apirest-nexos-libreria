import { Component, OnInit } from '@angular/core';
import { SharedService } from "src/app/shared.service";

@Component({
  selector: 'app-show-editorial',
  templateUrl: './show-editorial.component.html',
  styleUrls: ['./show-editorial.component.css']
})
export class ShowEditorialComponent implements OnInit {
  
  constructor(private service:SharedService) { }
  EditorialList:any=[];
  ModalTitle:string | undefined;
  ActiveAddEditEditorialComp:boolean=false;
  ed:any;  
  

  ngOnInit(): void {
    this.refreshEditList();
  }

  refreshEditList(){
    this.service.getEditList().subscribe(data=>{
      console.log(data);
      this.EditorialList=data;
    })
  }

  addClick(){
    this.ed={
      IdEditorial:0,
      nombreEditorial:"",
      direccionEditorial:"",
      telefonoEditorial:"",
      correoEditorial:""
    }
    this.ModalTitle="Ingresar Editorial!!";
    this.ActiveAddEditEditorialComp=true;
  }

  editClick(item){
    this.ed=item;
    this.ModalTitle="Editar Editorial";
    this.ActiveAddEditEditorialComp=true;

  }
  deleteClick(item){
    if(confirm('¿Seguro de Borrar esta Editorial?')){
      this.service.deletEdit(item.IdEditorial).subscribe(data=>{
        alert(data.toString());
        this.refreshEditList();
      })

    }
  }

  closeClick(){
    this.ActiveAddEditEditorialComp=false;
    this.refreshEditList();
  }


}
