import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEditorialComponent } from './show-editorial.component';

describe('ShowEditorialComponent', () => {
  let component: ShowEditorialComponent;
  let fixture: ComponentFixture<ShowEditorialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowEditorialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEditorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
