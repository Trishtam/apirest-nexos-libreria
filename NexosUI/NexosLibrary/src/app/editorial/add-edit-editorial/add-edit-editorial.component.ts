import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from "src/app/shared.service";

@Component({
  selector: 'app-add-edit-editorial',
  templateUrl: './add-edit-editorial.component.html',
  styleUrls: ['./add-edit-editorial.component.css']
})
export class AddEditEditorialComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() ed:any;
  IdEditorial:string|undefined;
  nombreEditorial: string  | undefined;
  direccionEditorial:string | undefined;
  telefonoEditorial:string | undefined;
  correoEditorial:string | undefined;
  maxLibRegistrado:string | undefined
  ngOnInit(): void {
    this.IdEditorial=this.ed.IdEditorial;
    this.nombreEditorial=this.ed.nombreEditorial;
    this.direccionEditorial=this.ed.direccionEditorial;
    this.telefonoEditorial=this.ed.telefonoEditorial;
    this.correoEditorial=this.ed.correoEditorial;
    this.maxLibRegistrado=this.ed.maxLibRegistrado;
  }
  addEditorial(){
    var val = {
      IdEditorial:this.IdEditorial,
      nombreEditorial:this.nombreEditorial,
      direccionEditorial:this.direccionEditorial,
      telefonoEditorial:this.telefonoEditorial,
      correoEditorial:this.correoEditorial,
      maxLibRegistrado:this.maxLibRegistrado,
    };
    this.service.addEdit(val).subscribe(res=>{
      alert(res.toString());
    });
  }
  updtEditorial(){
    var val = {
      IdEditorial:this.IdEditorial,
      nombreEditorial:this.nombreEditorial,
      direccionEditorial:this.direccionEditorial,
      telefonoEditorial:this.telefonoEditorial,
      correoEditorial:this.correoEditorial,
      maxLibRegistrado:this.maxLibRegistrado,
    };
    this.service.updtEdit(val).subscribe(res=>{
      alert(res.toString());
    });
  }

}
