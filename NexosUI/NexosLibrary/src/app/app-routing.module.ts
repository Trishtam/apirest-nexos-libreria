import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibroComponent } from "./libro/libro.component";
import { AutorComponent } from "./autor/autor.component";
import { EditorialComponent } from "./editorial/editorial.component";

const routes: Routes = [
{path:'Libro',component:LibroComponent},
{path:'Editorial',component:EditorialComponent},
{path:'Autor',component:AutorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
